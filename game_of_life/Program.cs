﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game_of_life
{
    class Program
    {
        static void Main(string[] args)
        {
            GameOfLife game = new GameOfLife();
            game.run();
        }
    }

    class GameOfLife
    {
        Board board;

        public GameOfLife()
        {
            board = new Board(20, 20);
        }

        public void run()
        {
            while (true)
            {
                // TODO: add delay between iterations
                System.Threading.Thread.Sleep(1000);
                Console.Clear();
                board.nextIteration();
                showBoard();
            }
        }

        public void showBoard()
        {
            for (int i = 0; i < board.getHeight(); ++i)
            {
                for (int j = 0; j < board.getWidth(); ++j)
                {
                    Cell cell = board.getBoardCell(i, j);
                    Console.Write(cell.getActualState() == StateType.alive ? '#' : ' ');
                }
                Console.Write('\n');
            }
        }
    }

    class Board
    {
        Cell[,] board;
        int height, width;

        public Board(int height, int width)
        {
            this.height = height;
            this.width = width;

            board = new Cell[height, width];
            initializeBoard();
        }

        public int getHeight()
        {
            return height;
        }

        public int getWidth()
        {
            return width;
        }

        private void initializeBoard()
        {
            Random rnd = new Random();
            for (int i = 0; i < height; ++i)
            {
                for (int j = 0; j < width; ++j)
                {
                    board[i, j] = new Cell();
                    board[i, j].setActualState(rnd.Next(0, 100)%2 != 0 ? StateType.alive : StateType.dead);
                }
            }
        }

        public int getAliveNeighboursNumber(int i, int j)
        {
            int aliveNeighCount = 0;
            if(i == 0 && j == 0)
            {
                aliveNeighCount = board[i+1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            else if(i == height-1 && j == 0)
            {
                aliveNeighCount = board[i-1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i-1,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            else if(i == 0 && j == width-1)
            {
                aliveNeighCount = board[i+1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            else if(i == height-1 && j == width-1)
            {
                aliveNeighCount = board[i-1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i-1,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            else if(i == 0)
            {
                aliveNeighCount = board[i+1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            else if(i == height-1)
            {
                aliveNeighCount = board[i-1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i-1,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i-1,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            else if(j == 0)
            {
                aliveNeighCount = board[i+1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i-1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i-1,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            else if(j == width-1)
            {
                aliveNeighCount = board[i-1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            else 
            {
                aliveNeighCount = board[i-1,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i-1,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i-1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j-1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i+1,j].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
                aliveNeighCount = board[i,j+1].getActualState() == StateType.alive? aliveNeighCount+1 : aliveNeighCount;
            }

            return aliveNeighCount;
        }

        public void nextIteration()
        {
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    int help = getAliveNeighboursNumber(i, j);
                    if (help < 2 || help > 3)
                        board[i, j].setFutureState(StateType.dead);
                    else if ((help==2 || help == 3) && board[i, j].getActualState()==StateType.alive )
                        board[i, j].setFutureState(StateType.alive);
                    else if(help == 3 && board[i, j].getActualState()==StateType.dead)
                        board[i, j].setFutureState(StateType.alive);
                }

            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    board[i, j].setActualState(board[i, j].getFutureState());
                }
        }

        public Cell getBoardCell(int i, int j)
        {
            return board[i,j];
        }
    }

    enum StateType { alive, dead };

    class Cell
    {
        StateType actualState = StateType.dead;
        StateType futureState = StateType.dead;

        public StateType getActualState()
        {
            return actualState;
        }

        public void setActualState(StateType actualState)
        {
            this.actualState = actualState;
        }

        public StateType getFutureState()
        {
            return futureState;
        }

        public void setFutureState(StateType futureState)
        {
            this.futureState = futureState;
        }
    }
}
